import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import tailwindcss from 'tailwindcss';

// https://vitejs.dev/config/
// @ts-ignore
export default defineConfig({
  plugins: [svelte(), tailwindcss],
});
